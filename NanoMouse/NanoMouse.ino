const byte ledpin = 13;
const byte buttonpin = 9;  

void setup()
{  
  pinMode(ledpin,OUTPUT);
  pinMode(buttonpin,INPUT_PULLUP);

  while(digitalRead(buttonpin))
  {}
}

void loop() 
{  
  digitalWrite(ledpin,HIGH); 
}